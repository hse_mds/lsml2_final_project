FROM python:3.11.7-slim-bullseye

RUN apt-get update && apt-get install -y gcc python3-dev

WORKDIR /app
ENV PYTHONPATH=/app

COPY ./requirements.txt /app/requirements.txt
RUN pip install --upgrade pip && pip install -r /app/requirements.txt

COPY . /app/

EXPOSE 80
ENTRYPOINT ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "80"]