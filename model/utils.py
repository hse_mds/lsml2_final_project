import io
from time import time
from copy import deepcopy

import requests
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import torch.nn.functional as F

from sklearn.metrics import accuracy_score, f1_score

from model.StdConv2d import StdConv2d


if torch.cuda.is_available():
    DEVICE = torch.device('cuda')
elif torch.backends.mps.is_available():
    DEVICE = torch.device('mps')
else:
    DEVICE = torch.device('cpu')


def get_weights(bit_variant):
    response = requests.get(f'https://storage.googleapis.com/bit_models/{bit_variant}.npz')
    response.raise_for_status()
    return np.load(io.BytesIO(response.content))


def conv3x3(cin, cout, stride=1, groups=1, bias=False):
    return StdConv2d(cin, cout, kernel_size=3, stride=stride, padding=1, bias=bias, groups=groups)


def conv1x1(cin, cout, stride=1, bias=False):
    return StdConv2d(cin, cout, kernel_size=1, stride=stride, padding=0, bias=bias)


def tf2th(conv_weights):
    """Possibly convert HWIO to OIHW"""
    if conv_weights.ndim == 4:
        conv_weights = np.transpose(conv_weights, [3, 2, 0, 1])
    return torch.from_numpy(conv_weights)


def test_model(model, test_loader):
    model = model.to(DEVICE)
    model.eval()
    all_labels = []
    all_predictions = []

    with torch.no_grad():
        for inputs, labels in test_loader:
            inputs, labels = inputs.to(DEVICE), labels.to(DEVICE)
            outputs = model(inputs)
            _, predicted = torch.max(outputs.data, 1)
            all_labels.extend(labels.cpu().numpy())
            all_predictions.extend(predicted.cpu().numpy())

    test_f1 = f1_score(all_labels, all_predictions)
    test_accuracy = accuracy_score(all_labels, all_predictions)
    print(f'F1 score: {test_f1:.4f}')
    print(f'Accuracy: {test_accuracy:.4f}\n')
    return test_f1, test_accuracy


def train_model(model, train_loader, val_loader=None, **kwargs):
    new_model = deepcopy(model)
    new_model = new_model.to(DEVICE)

    num_epochs = kwargs.get('num_epochs') or 15
    criterion = kwargs.get('criterion') or nn.CrossEntropyLoss()
    if kwargs.get('optimizer') == 'Adam':
        optimizer = optim.Adam(
            new_model.parameters(),
            lr=kwargs.get('optimizer_lr') or 0.001,
        )
    else:
        optimizer = optim.SGD(
            new_model.parameters(),
            lr=kwargs.get('optimizer_lr') or 0.001,
            momentum=kwargs.get('sgd_momentum') or 0.9,
        )

    scheduler = kwargs.get('scheduler') or lr_scheduler.StepLR(
        optimizer,
        step_size=kwargs.get('scheduler_step_size') or (num_epochs // 2),
        gamma=kwargs.get('scheduler_gamma') or 0.1
    )

    train_losses = []
    val_accuracies = []
    val_f1s = []
    times = []
    best_model = new_model.state_dict()
    best_f1 = 0.0
    for epoch in range(num_epochs):
        start = time()
        print(f'Epoch {epoch + 1}')
        new_model.train()
        epoch_loss = []
        for step, data in enumerate(train_loader):
            inputs, labels = data
            inputs, labels = inputs.to(DEVICE), labels.to(DEVICE)
            optimizer.zero_grad()
            outputs = new_model(inputs)
            loss = criterion(outputs, labels)
            epoch_loss.append(loss.cpu().detach().numpy())
            loss.backward()
            optimizer.step()
            if step % 10 == 0:
                print(f'\tStep: {step}, Loss: {loss:.4f}')

        times.append(time() - start)
        train_losses.append(np.mean(epoch_loss))
        scheduler.step()

        if val_loader:
            print('Start evaluating')
            val_f1, val_accuracy = test_model(new_model, val_loader)
            val_f1s.append(val_f1)
            val_accuracies.append(val_accuracy)
            if val_f1 > best_f1:
                best_model = new_model.state_dict()
                best_f1 = val_f1

    return new_model, np.mean(times), best_model, val_f1s, val_accuracies, train_losses
