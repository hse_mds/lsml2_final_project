import os
import requests
from random import randint


def test_app():
    url = 'http://localhost:8000/classify'

    classes = ['AiArtData', 'RealArt']
    chosen_class = classes[randint(0, len(classes) - 1)]
    dir_path = f'./data/raw_data/archive/{chosen_class}/{chosen_class}'
    if not os.path.exists(dir_path):
        print('No directory')
        return
    images = [file for file in os.listdir(dir_path) if (file.endswith('.png') or file.endswith('.jpg'))]
    image_path = os.path.join(dir_path, images[randint(0, len(images) - 1)])
    print('Chosen image:', image_path)

    with open(image_path, 'rb') as file:
        response = requests.post(url, files={'file': file})
    response.raise_for_status()
    print(response.json())


if __name__ == '__main__':
    test_app()