import os
import io
from PIL import Image

import torch
import torchvision.transforms as transforms
import neptune
from fastapi import FastAPI, UploadFile, File

from model.BiT import ResNetV2

app = FastAPI()

def get_model(model_id='HSE-ADAMCPU'):
    model = ResNetV2(ResNetV2.BLOCK_UNITS['r50'], width_factor=1, head_size=2, zero_head=True)
    if not os.path.exists('./mlops/best_model.pt'):
        nep_model = neptune.init_model(with_id=model_id)
        if nep_model.exists("model/bin"):
            nep_model["model/bin"].download('./mlops/best_model.pt')
    model.load_state_dict(torch.load('./mlops/best_model.pt'))
    # model = model.to('cpu')
    return model


def preprocess_image(image_bytes):
    image = Image.open(io.BytesIO(image_bytes))
    if image.mode != "RGB":
        image = image.convert("RGB")

    transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])

    image_tensor = transform(image).unsqueeze(0)

    return image_tensor


@app.get('/')
async def root():
    return {"message": "Use `/classify` method."}


@app.post("/classify")
async def classify_image(file: UploadFile = File(...)):
    model = get_model()
    contents = await file.read()

    image_tensor = preprocess_image(contents)

    with torch.no_grad():
        model.eval()
        outputs = model(image_tensor)

    predicted_class = torch.argmax(outputs).item()
    return {"predicted_class": 'Real Art' if predicted_class else 'AI Generated'}
