import os

import torch
from torch.utils.data import DataLoader, random_split

from torchvision import transforms
from torchvision.datasets import ImageFolder

RANDOM_SEED = 42
torch.manual_seed(RANDOM_SEED)


def load_dataset():
    current_dir = os.path.dirname(__file__)
    imgs_path = os.path.join(current_dir, 'raw_data', 'archive')

    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=mean, std=std)
    ])

    dataset = ImageFolder(imgs_path, transform=transform)

    return dataset


def split_dataset(dataset, train_size=0.7, val_size=0.15):
    train = int(train_size * len(dataset))
    val = int(val_size * len(dataset))
    test = len(dataset) - train - val

    train_dataset, val_dataset, test_dataset = random_split(dataset, [train, val, test])
    print(f'Lengths:\nTrain = {len(train_dataset)}, Val = {len(val_dataset)}, Test = {len(test_dataset)}')

    return train_dataset, val_dataset, test_dataset



def get_loaders(batch_size):
    train_dataset, val_dataset, test_dataset = split_dataset(load_dataset())

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

    return train_loader, val_loader, test_loader
