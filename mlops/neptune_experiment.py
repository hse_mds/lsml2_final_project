import os

import torch
from torch.utils.data import DataLoader
import neptune

from model.utils import train_model, test_model
from data.preprocessing import get_loaders, split_dataset, load_dataset


def run_experiment(model, model_params, batch_size):
    run = neptune.init_run(
        project=os.getenv("NEPTUNE_PROJECT"),
        api_token=os.getenv("NEPTUNE_API_TOKEN")
    )
    train_loader, val_loader, test_loader = get_loaders(batch_size)
    run['batch_size'] = batch_size

    run['model/parameters'] = model_params
    final_model, train_time, best_model, val_f1s, val_accuracies, train_losses = (
        train_model(model, train_loader, val_loader=val_loader, **model_params)
    )
    run['train/avg_epoch_time'] = train_time
    for f1, acc, loss in zip(val_f1s, val_accuracies, train_losses):
        run['val/f1_score'].append(f1)
        run['val/accuracy'].append(acc)
        run['train/loss'].append(loss)

    f1_score, accuracy = test_model(final_model, test_loader)
    run['test/f1_score'] = f1_score
    run['test/accuracy'] = accuracy

    run.stop()


def create_model(model, model_params, batch_size):
    model_name = (model_params.get('optimizer') + 'CPU').upper()  # TODO
    nep_model = neptune.init_model(
        key=model_name,
    )
    nep_model['model'] = model_params
    train_dataset, _, test_dataset = split_dataset(load_dataset(), train_size=0.85, val_size=0.0)
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

    final_model, _, _, _, _, _ = train_model(model, train_loader, **model_params)
    _, _ = test_model(final_model, test_loader)

    final_model.to('cpu')
    torch.save(final_model.state_dict(), 'best_model.pt')
    nep_model['model/bin'].upload('best_model.pt')

    nep_model.stop()


if __name__ == '__main__':
    import numpy as np
    import scipy.stats as scs

    from model.BiT import ResNetV2
    from model.utils import get_weights

    import warnings

    warnings.filterwarnings("ignore")


    def init_model():
        model = ResNetV2(ResNetV2.BLOCK_UNITS['r50'], width_factor=1, head_size=2, zero_head=True)
        project_path = '/Users/egor/Edu/HSE/LSML/lsml2_final_project'
        if os.path.exists(os.path.join(project_path, 'model/weights/BiT-M-R50x1.npz')):
            weights = np.load(os.path.join(project_path, 'model/weights/BiT-M-R50x1.npz'))
        else:
            weights = get_weights('BiT-M-R50x1')
        model.load_from(weights)
        return model

    # optimizers = ['SGD', 'Adam']
    # optimizer_lr = scs.loguniform(1e-5, 0.001)
    # sgd_momentum = np.arange(0.85, 1., 0.03)
    # batch_sizes = [16, 32]
    # num_epochs = [6, 8]
    #
    # for _ in range(20): # c 41
    #     model = init_model()
    #     model_params = {
    #         'optimizer': np.random.choice(optimizers),
    #         'optimizer_lr': optimizer_lr.rvs(),
    #         'sgd_momentum': np.random.choice(sgd_momentum),
    #         'num_epochs': np.random.choice(num_epochs),
    #     }
    #     run_experiment(model, model_params, batch_size=int(np.random.choice(batch_sizes)))

    final_params = [
        (
            {
                'num_epochs': 10,
                'optimizer': 'Adam',
                'optimizer_lr': 0.00001,
            }, 32),
    ]
    for model_params, batch_size in final_params:
        model = init_model()
        create_model(model, model_params, batch_size)
