Application uses BiT model for classification images into two classes - AI Generated and Real Art.

Dataset "AI Generated Images vs Real Images" from [kaggle](https://www.kaggle.com/datasets/cashbowman/ai-generated-images-vs-real-images)

Code for BiT model:
- [github](https://github.com/google-research/big_transfer/tree/master?tab=readme-ov-file)
- [colab](https://colab.research.google.com/github/google-research/big_transfer/blob/master/colabs/big_transfer_pytorch.ipynb#scrollTo=y00wwFQvrwsX)

## Quick start
### 1. Repo
1. Pull git repository locally;
2. Build image `docker build -t lmsl2-final-project:latest .`
3. Run container `docker run -p 80:80 --rm lmsl2-final-project:latest`
4. Classify images with application using `curl -X POST -F "file=@<IMAGE_PATH>" http://127.0.0.1:80/classify`.
Replace <IMAGE_PATH> with path to image which you want to classify.

### 2. Container registry <DRAFT>
You can pull ready docker image from gitlab container registry. Contact me if needed.
